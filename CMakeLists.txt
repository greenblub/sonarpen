project(sonarpen LANGUAGES C CXX ASM)
cmake_minimum_required(VERSION 3.10)

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    add_definitions( -DDEBUG_MODE )
endif()

set(SONARPEN_NATIVE ${PROJECT_NAME})
set(SONARPEN_NATIVE_SOURCES cpp/sonarpen_wrapper.cpp)
add_library(${SONARPEN_NATIVE} SHARED ${SONARPEN_NATIVE_SOURCES})
target_link_libraries(${SONARPEN_NATIVE}
        PRIVATE
        android
        log)

install(TARGETS ${SONARPEN_NATIVE}
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
)

install(DIRECTORY ${CMAKE_SOURCE_DIR}/java/src DESTINATION  ${CMAKE_INSTALL_PREFIX}/src/android/java)
install(FILES ${CMAKE_SOURCE_DIR}/aar/sonarpen.aar DESTINATION ${CMAKE_INSTALL_PREFIX}/aar)
